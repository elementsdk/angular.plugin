# CakePHP - Angular Plugin

Angular plugin is a CakePHP 3.x plugin with a few convinience helpers and components to make integrating a CakePHP backend and Angular frontend simpler and easier

# Prerequisites

A working `node.js` install

# Installation

Add this to your `composer.json` file:

```

"require": {
        "elementsdk/angular.plugin" : "dev-master"
        ...
	},
...
"scripts": {
		"post-install-cmd": "cd Plugin/Angular/webroot; npm install",
        "post-update-cmd" : "cd Plugin/Angular/webroot; npm install"
        ...
	},
```


While in `Plugin/Angular/webroot`

run `npm install`

# Usage

# Documentation

# Todo
* Usage docs

See the API inline documentation.


# Version history
* 3.0.0
    * First release for CakePHP 3.x

* 1.0.0
    * First release