<?php

App::uses('AppHelper', 'Helper');


/**
 * Class AngularHelper
 *
 * @package Angular\View\Helper
 */
class AngularHelper extends AppHelper {

	public $helpers = [
		'Form',
		'Html'];


	/**
	 * @param Event $event
	 * @param $viewFile
	 */
	public function beforeRender($viewFile) {
		$this->ngBootstrap();
	}


	/**
	 *
	 */
	public function ngBootstrap() {

		$this->Html->css([
			'Angular./bower_components/bootstrap/dist/css/bootstrap.min.css',
			'Angular./bower_components/bootstrap/dist/css/bootstrap-theme.css'], ['block' => 'css']);

		$this->Html->script([
			'//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js',
			'Angular./bower_components/bootstrap/dist/js/bootstrap.min.js',
			'Angular./bower_components/angular/angular.js',
			'Angular./bower_components/angular-route/angular-route.js'], ['block' => 'script']);
	}

	/**
	 * @param $fieldName
	 * @param array $options
	 * @return mixed
	 */
	public function ngInput($fieldName, array $options = []) {
		$options = array_merge(['ng-model' => $fieldName], $options);
		return $this->Form->input($fieldName, $options);

	}

}
